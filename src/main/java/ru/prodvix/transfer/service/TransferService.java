package ru.prodvix.transfer.service;

import ru.prodvix.transfer.exception.ServiceException;
import ru.prodvix.transfer.model.MessageIn;

public interface TransferService {

	void transfer(MessageIn messageIn) throws ServiceException;
}

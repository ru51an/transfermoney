package ru.prodvix.transfer.service;

import ru.prodvix.transfer.locker.Locker;
import ru.prodvix.transfer.exception.ServiceException;
import ru.prodvix.transfer.model.Account;
import ru.prodvix.transfer.model.MessageIn;
import ru.prodvix.transfer.repository.AccountRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.locks.ReentrantLock;

public class TransferServiceImpl implements TransferService {

	private final static int MINIMUM_AMOUNT = 1;
	private final static Locker locker = Locker.getInstance();
	private AccountRepository accountRepository;

	public TransferServiceImpl(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	@Override
	public void transfer(MessageIn messageIn) throws ServiceException {
		validateMessageIn(messageIn);

		ReentrantLock senderLock = locker.getLock(messageIn.getSenderAccNum());
		ReentrantLock recipientLock = locker.getLock(messageIn.getRecipientAccNum());

		try {
			// Changing the order of locking is for avoiding a deadlock
			if (messageIn.getSenderAccNum() < messageIn.getRecipientAccNum()) {
				senderLock.lock();
				recipientLock.lock();
			} else {
				recipientLock.lock();
				senderLock.lock();
			}

			// Getting sender account from a storage
			Account senderAccount = accountRepository.getAccountByAccNum(messageIn.getSenderAccNum());
			if (senderAccount == null) {
				throw new ServiceException("Account " + messageIn.getSenderAccNum() + " doesn't exist");
			} else if (senderAccount.getBalance().compareTo(messageIn.getAmount()) < 0) {
				throw new ServiceException("There is no enough money on account number " + messageIn.getSenderAccNum());
			}

			BigDecimal roundedAmount = messageIn.getAmount().setScale(2, RoundingMode.FLOOR);

			// Creating new sender account object and filling its fields with updated data
			Account updatedSenderAccount = new Account();
			updatedSenderAccount.setAccNum(senderAccount.getAccNum());
			updatedSenderAccount.setBalance(senderAccount.getBalance().subtract(roundedAmount));

			// Getting recipient account from the storage
			Account recipientAccount = accountRepository.getAccountByAccNum(messageIn.getRecipientAccNum());
			if (recipientAccount == null) {
				throw new ServiceException("Account " + messageIn.getRecipientAccNum() + " doesn't exist");
			}

			// Creating new recipient account object and filling its fields with updated data
			Account updatedRecipientAccount = new Account();
			updatedRecipientAccount.setAccNum(recipientAccount.getAccNum());
			updatedRecipientAccount.setBalance(recipientAccount.getBalance().add(roundedAmount));

			// Updating sender account in the storage
			accountRepository.saveAccount(updatedSenderAccount);

			// Updating recipient account in the storage.
			accountRepository.saveAccount(updatedRecipientAccount);
		} finally {
			senderLock.unlock();
			recipientLock.unlock();
		}
	}

	private void validateMessageIn(MessageIn messageIn) throws ServiceException {
		if (messageIn.getSenderAccNum() == messageIn.getRecipientAccNum()) {
			throw new ServiceException("Sender and recipient account numbers are the same");
		} else if (messageIn.getAmount() == null ) {
			throw new ServiceException("The amount to transfer is absent");
		} else if (BigDecimal.valueOf(MINIMUM_AMOUNT).compareTo(messageIn.getAmount()) > 0) {
			throw new ServiceException("The amount is less than " + MINIMUM_AMOUNT);
		}
	}
}
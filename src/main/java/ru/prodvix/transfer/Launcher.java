package ru.prodvix.transfer;

import org.eclipse.jetty.server.Server;
import ru.prodvix.transfer.handler.RequestHandler;

public class Launcher {

	private static final int PORT = 8080;

	public static void main(String[] args) throws Exception {

		// Configuring the server
		Server server = new Server(PORT);
		server.setHandler(new RequestHandler());

		// Starting the server
		server.start();
		server.join();
	}
}
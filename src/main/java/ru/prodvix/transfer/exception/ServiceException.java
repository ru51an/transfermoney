package ru.prodvix.transfer.exception;

public class ServiceException extends Exception {

	public ServiceException(String message) {
		super(message);
	}
}

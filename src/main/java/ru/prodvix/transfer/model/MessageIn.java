package ru.prodvix.transfer.model;

import java.math.BigDecimal;

public class MessageIn {

	private String requestId;
	private long senderAccNum;
	private long recipientAccNum;
	private BigDecimal amount;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public long getSenderAccNum() {
		return senderAccNum;
	}

	public void setSenderAccNum(long senderAccNum) {
		this.senderAccNum = senderAccNum;
	}

	public long getRecipientAccNum() {
		return recipientAccNum;
	}

	public void setRecipientAccNum(long recipientAccNum) {
		this.recipientAccNum = recipientAccNum;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}

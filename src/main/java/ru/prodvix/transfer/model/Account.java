package ru.prodvix.transfer.model;

import java.math.BigDecimal;

public class Account {

	private long accNum;
	private BigDecimal balance;

	public long getAccNum() {
		return accNum;
	}

	public void setAccNum(long accNum) {
		this.accNum = accNum;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}

package ru.prodvix.transfer.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MessageOut {

	private static Gson gson = new GsonBuilder().create();

	private String requestId;
	private boolean success;
	private String error;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return gson.toJson(this);
	}
}

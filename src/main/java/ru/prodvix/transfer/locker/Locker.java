package ru.prodvix.transfer.locker;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class Locker {

	private final static Locker instance = new Locker();
	private Map<Long, ReentrantLock> locks;

	private Locker() {
		locks = new ConcurrentHashMap<>();
	}

	public static Locker getInstance() {
		return instance;
	}

	public ReentrantLock getLock(long key) {
		ReentrantLock initValue = new ReentrantLock();
		ReentrantLock lock = locks.putIfAbsent(key, initValue);
		if (lock == null) {
			return initValue;
		}
		return lock;
	}
}
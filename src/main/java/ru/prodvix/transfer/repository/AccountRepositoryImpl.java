package ru.prodvix.transfer.repository;

import ru.prodvix.transfer.model.Account;

public class AccountRepositoryImpl implements AccountRepository {

	private Storage storage;

	public AccountRepositoryImpl(Storage storage) {
		this.storage = storage;
	}

	@Override
	public Account getAccountByAccNum(long accountNumber) {
		return storage.getAccount(accountNumber);
	}

	@Override
	public void saveAccount(Account account) {
		storage.saveAccount(account.getAccNum(), account);
	}
}

package ru.prodvix.transfer.repository;

import ru.prodvix.transfer.model.Account;

public interface Storage {

	Account getAccount(long key);

	void saveAccount(long key, Account value);
}

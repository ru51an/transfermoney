package ru.prodvix.transfer.repository;

import ru.prodvix.transfer.model.Account;

public interface AccountRepository {

	Account getAccountByAccNum(long accountNumber);

	void saveAccount(Account account);
}

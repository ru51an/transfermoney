package ru.prodvix.transfer.repository;

import ru.prodvix.transfer.model.Account;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StorageImpl implements Storage {

	private Map<Long, Account> accounts = new ConcurrentHashMap<>();

	@Override
	public Account getAccount(long key) {
		return accounts.get(key);
	}

	@Override
	public void saveAccount(long key, Account value) {
		accounts.put(key, value);
	}
}

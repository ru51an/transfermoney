package ru.prodvix.transfer.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.prodvix.transfer.model.Account;
import ru.prodvix.transfer.model.MessageIn;
import ru.prodvix.transfer.model.MessageOut;
import ru.prodvix.transfer.repository.AccountRepository;
import ru.prodvix.transfer.repository.AccountRepositoryImpl;
import ru.prodvix.transfer.repository.Storage;
import ru.prodvix.transfer.repository.StorageImpl;
import ru.prodvix.transfer.service.TransferService;
import ru.prodvix.transfer.service.TransferServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

public class RequestHandler extends AbstractHandler {

	private Logger log = LoggerFactory.getLogger(RequestHandler.class);
	private Gson gson = new GsonBuilder().create();
	private TransferService transferService;

	public RequestHandler() {
		Storage storage = initAndFillData();
		AccountRepository accountRepository = new AccountRepositoryImpl(storage);
		transferService = new TransferServiceImpl(accountRepository);
	}

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// Getting the request method
		String method = baseRequest.getMethod();

		log.debug("Request method is {}", method);
		log.debug("Request path is {}", target);

		MessageOut messageOut = new MessageOut();

		// Checking if requested path equals '/transfer' or '/transfer/'
		if ("/transfer".equals(target) || "/transfer/".equals(target)) {

			// Check if method is POST
			if ("POST".equals(method)) {
				String body = getBodyAsString(baseRequest.getInputStream());
				log.debug("Request body is {}", body);

				try {
					MessageIn messageIn = convertFromJson(body);

					if (messageIn == null) {
						log.error("The request body is null");
						messageOut.setSuccess(false);
						messageOut.setError("The request body is null");
						response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					} else {
						messageOut.setRequestId(messageIn.getRequestId());
						transferService.transfer(messageIn);

						messageOut.setRequestId(messageIn.getRequestId());
						messageOut.setSuccess(true);
						response.setStatus(HttpServletResponse.SC_OK);
					}
				} catch (Exception e) {
					log.error("Error. MSG = {}", e.getMessage(), e);
					messageOut.setSuccess(false);
					messageOut.setError(e.getMessage());
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
			} else {
				log.error("The method is {}. POST is expected.", method);
				messageOut.setSuccess(false);
				messageOut.setError("The method is " + method + ". POST is expected.");
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
		} else {
			log.error("Requested path '{}' is not handled.", target);
			messageOut.setSuccess(false);
			messageOut.setError("Requested path '" + target + "' is not handled.");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}

		response.setContentType("application/json;charset=utf-8");
		response.getWriter().print(messageOut);

		log.info("The response is {}", messageOut);

		baseRequest.setHandled(true);
	}

	/**
	 * The method converts InputStream to String.
	 *
	 * @param is InputStream of a request
	 * @return The request body as String
	 * @throws IOException
	 */
	String getBodyAsString(InputStream is) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;

		while ((length = is.read(buffer)) != -1) {
			outputStream.write(buffer, 0, length);
		}

		return outputStream.toString("UTF-8");
	}

	MessageIn convertFromJson(String body) {
		return gson.fromJson(body, MessageIn.class);
	}

	private Storage initAndFillData() {
		Storage storage = new StorageImpl();

		Account account1 = new Account();
		account1.setAccNum(11);
		account1.setBalance(new BigDecimal("1000.00"));
		storage.saveAccount(account1.getAccNum(), account1);

		Account account2 = new Account();
		account2.setAccNum(22);
		account2.setBalance(new BigDecimal("2000.00"));
		storage.saveAccount(account2.getAccNum(), account2);

		Account account3 = new Account();
		account3.setAccNum(33);
		account3.setBalance(new BigDecimal("3000.00"));
		storage.saveAccount(account3.getAccNum(), account3);

		Account account4 = new Account();
		account4.setAccNum(44);
		account4.setBalance(new BigDecimal("4000.00"));
		storage.saveAccount(account4.getAccNum(), account4);

		Account account5 = new Account();
		account5.setAccNum(55);
		account5.setBalance(new BigDecimal("5000.00"));
		storage.saveAccount(account5.getAccNum(), account5);

		return storage;
	}
}

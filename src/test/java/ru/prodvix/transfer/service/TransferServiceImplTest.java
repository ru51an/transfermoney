package ru.prodvix.transfer.service;

import org.junit.Before;
import org.junit.Test;
import ru.prodvix.transfer.exception.ServiceException;
import ru.prodvix.transfer.model.Account;
import ru.prodvix.transfer.model.MessageIn;
import ru.prodvix.transfer.repository.AccountRepository;
import ru.prodvix.transfer.repository.AccountRepositoryImpl;
import ru.prodvix.transfer.repository.Storage;
import ru.prodvix.transfer.repository.StorageImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TransferServiceImplTest {

	private Storage storage;
	private TransferServiceImpl service;
	private static final Executor executor = Executors.newFixedThreadPool(500);

	private final static long ACC_NUM_1 = 11;
	private final static long ACC_NUM_2 = 22;
	private final static long ACC_NUM_3 = 33;
	private final static long ACC_NUM_4 = 44;
	private final static long ACC_NUM_5 = 55;
	private final static long ACC_NUM_6 = 66;

	private final static BigDecimal BALANCE_1 = new BigDecimal("1000.00").setScale(2, RoundingMode.FLOOR);
	private final static BigDecimal BALANCE_2 = new BigDecimal("2000.00").setScale(2, RoundingMode.FLOOR);
	private final static BigDecimal BALANCE_3 = new BigDecimal("3000.00").setScale(2, RoundingMode.FLOOR);
	private final static BigDecimal BALANCE_4 = new BigDecimal("4000.00").setScale(2, RoundingMode.FLOOR);
	private final static BigDecimal BALANCE_5 = new BigDecimal("5000.00").setScale(2, RoundingMode.FLOOR);
	private final static BigDecimal BALANCE_6 = new BigDecimal("6000.00").setScale(2, RoundingMode.FLOOR);

	private static AtomicInteger exceptionCaughtNumber = new AtomicInteger(0);
	private static AtomicInteger exceptionCyclesCaughtNumber = new AtomicInteger(0);

	@Before
	public void init() {
		storage = initAndFillData();
		AccountRepository accountRepository = new AccountRepositoryImpl(storage);
		service = new TransferServiceImpl(accountRepository);
	}

	@Test
	public void transferFromZeroSenderAccount() {
		MessageIn messageIn = createMessageIn(0, ACC_NUM_1, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Account 0 doesn't exist", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferFromNegativeSenderAccount() {
		MessageIn messageIn = createMessageIn(-10, ACC_NUM_1, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Account -10 doesn't exist", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferToZeroRecipientAccount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, 0, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Account 0 doesn't exist", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferToNegativeRecipientAccount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, -20, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Account -20 doesn't exist", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferFromZeroAccNumToZeroAccNum() {
		MessageIn messageIn = createMessageIn(0, 0, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Sender and recipient account numbers are the same", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferToSameAccNum() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_1, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Sender and recipient account numbers are the same", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferFromNotExistingAccount() {
		MessageIn messageIn = createMessageIn(123, ACC_NUM_2, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Account 123 doesn't exist", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferToNotExistingAccount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, 123, new BigDecimal("100"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("Account 123 doesn't exist", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferNoAmount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, null);
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("The amount to transfer is absent", e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferNegativeAmount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, new BigDecimal("-100.00"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("The amount is less than " + 1, e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferZeroAmount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, new BigDecimal("0.00"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("The amount is less than " + 1, e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferLessThanMinAmount() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, new BigDecimal("0.99"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("The amount is less than " + 1, e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferCloseToMinAmountFromLeft() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, new BigDecimal("0.9999999999999999999999999999"));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("The amount is less than " + 1, e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferCloseToMinAmountFromRight() throws ExecutionException, InterruptedException {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, new BigDecimal("1.00000000000000000000000000000001"));

		transfer(1, messageIn);

		assertEquals(BALANCE_1.subtract(new BigDecimal("1.00")), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(new BigDecimal("1.00")), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void transferMinAmount() throws ExecutionException, InterruptedException {
		BigDecimal amount = new BigDecimal("1");
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		transfer(1, messageIn);

		assertEquals(BALANCE_1.subtract(amount), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amount), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void transferMoreThanHave() {
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, BALANCE_1.add(new BigDecimal("100")));
		boolean exceptionCaught = false;

		try {
			service.transfer(messageIn);
		} catch (Exception e) {
			assertEquals(ServiceException.class, e.getClass());
			assertEquals("There is no enough money on account number " + ACC_NUM_1, e.getMessage());
			exceptionCaught = true;
		}

		assertTrue(exceptionCaught);
		assertNoChanges();
	}

	@Test
	public void transferIntegerAmount() throws ExecutionException, InterruptedException {
		BigDecimal amount = new BigDecimal("100");
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		transfer(1, messageIn);

		assertEquals(BALANCE_1.subtract(amount), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amount), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void transferAmountWithTwoDecimalDigits() throws ExecutionException, InterruptedException {
		BigDecimal amount = new BigDecimal("100.25");
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		transfer(1, messageIn);

		assertEquals(BALANCE_1.subtract(amount), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amount), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void transferAmountWithThreeDecimalDigits() throws ExecutionException, InterruptedException {
		BigDecimal amount = new BigDecimal("100.255");
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		transfer(1, messageIn);

		assertEquals(BALANCE_1.subtract(new BigDecimal("100.25")), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(new BigDecimal("100.25")), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void transferAmountWithFourDecimalDigits() throws ExecutionException, InterruptedException {
		BigDecimal amount = new BigDecimal("100.2495");
		MessageIn messageIn = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		transfer(1, messageIn);

		assertEquals(BALANCE_1.subtract(new BigDecimal("100.24")), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(new BigDecimal("100.24")), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void twoTransfers() throws InterruptedException, ExecutionException {
		BigDecimal amount1 = new BigDecimal("100");
		BigDecimal amount2 = new BigDecimal("150");
		MessageIn messageIn1 = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount1);
		MessageIn messageIn2 = createMessageIn(ACC_NUM_2, ACC_NUM_3, amount2);

		transfer(1, messageIn1, messageIn2);

		assertEquals(BALANCE_1.subtract(amount1), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amount1).subtract(amount2), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3.add(amount2), getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void fiveTransfers() throws InterruptedException, ExecutionException {
		BigDecimal amount1 = new BigDecimal("100");
		BigDecimal amount2 = new BigDecimal("150");
		BigDecimal amount3 = new BigDecimal("150");
		BigDecimal amount4 = new BigDecimal("150");
		BigDecimal amount5 = new BigDecimal("150");
		MessageIn messageIn1 = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount1);
		MessageIn messageIn2 = createMessageIn(ACC_NUM_1, ACC_NUM_3, amount2);
		MessageIn messageIn3 = createMessageIn(ACC_NUM_2, ACC_NUM_4, amount3);
		MessageIn messageIn4 = createMessageIn(ACC_NUM_2, ACC_NUM_5, amount4);
		MessageIn messageIn5 = createMessageIn(ACC_NUM_3, ACC_NUM_2, amount5);

		transfer(1, messageIn1, messageIn2, messageIn3, messageIn4, messageIn5);

		assertEquals(BALANCE_1.subtract(amount1).subtract(amount3), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amount1).add(amount5).subtract(amount2).subtract(amount4), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3.add(amount3).subtract(amount5), getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4.add(amount3), getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5.add(amount4), getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void nRepeatedTransfers() throws InterruptedException, ExecutionException {
		int cyclesNum = 800;
		BigDecimal amount1 = new BigDecimal("1.111");
		BigDecimal amount2 = new BigDecimal("2.222");

		MessageIn messageIn1 = createMessageIn(ACC_NUM_3, ACC_NUM_1, amount1);
		MessageIn messageIn2 = createMessageIn(ACC_NUM_4, ACC_NUM_1, amount1);
		MessageIn messageIn3 = createMessageIn(ACC_NUM_3, ACC_NUM_2, amount2);
		MessageIn messageIn4 = createMessageIn(ACC_NUM_4, ACC_NUM_2, amount2);

		transfer(cyclesNum, messageIn1, messageIn2, messageIn3, messageIn4);

		BigDecimal amountRounded1 = new BigDecimal("1.11");
		BigDecimal amountRounded2 = new BigDecimal("2.22");

		assertEquals(BALANCE_1.add(amountRounded1.add(amountRounded1).multiply(new BigDecimal(cyclesNum))),
				getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amountRounded2.add(amountRounded2).multiply(new BigDecimal(cyclesNum))),
				getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3.subtract(amountRounded1.add(amountRounded2).multiply(new BigDecimal(cyclesNum))),
				getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4.subtract(amountRounded1.add(amountRounded2).multiply(new BigDecimal(cyclesNum))),
				getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void nCycleRepeatedTransfers() throws InterruptedException, ExecutionException {
		BigDecimal amount = new BigDecimal("1.1554");

		MessageIn messageIn1 = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);
		MessageIn messageIn2 = createMessageIn(ACC_NUM_2, ACC_NUM_3, amount);
		MessageIn messageIn3 = createMessageIn(ACC_NUM_3, ACC_NUM_4, amount);
		MessageIn messageIn4 = createMessageIn(ACC_NUM_4, ACC_NUM_5, amount);
		MessageIn messageIn5 = createMessageIn(ACC_NUM_5, ACC_NUM_6, amount);
		MessageIn messageIn6 = createMessageIn(ACC_NUM_6, ACC_NUM_1, amount);

		transfer(900, messageIn1, messageIn2, messageIn3, messageIn4, messageIn5, messageIn6);

		assertNoChanges();
	}

	@Test
	public void nCycleRepeatedTransfersFromOneToAnother() throws InterruptedException, ExecutionException {
		BigDecimal amount = new BigDecimal("1.01999");

		MessageIn messageIn1 = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		int cyclesNum = 900;
		transfer(cyclesNum, messageIn1);

		BigDecimal amountRounded = new BigDecimal("1.01");

		assertEquals(BALANCE_1.subtract(amountRounded.multiply(new BigDecimal(cyclesNum))), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(amountRounded.multiply(new BigDecimal(cyclesNum))), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void nTransfersFromOneToAnotherExceedingLimit() throws InterruptedException, ExecutionException {
		BigDecimal amount = new BigDecimal("1.00");

		MessageIn messageIn1 = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount);

		List<CompletableFuture<Void>> futures = new ArrayList<>();

		for (int i = 0; i < 1001; i++) {
			futures.add(transferWithException(messageIn1));
		}

		for (CompletableFuture<Void> future : futures) {
			future.get();
		}

		assertEquals(1, exceptionCaughtNumber.get());
		assertEquals(new BigDecimal("0.00"), getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2.add(BALANCE_1), getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	@Test
	public void nCycleTransfersFromOneToAnotherExceedingLimit() throws InterruptedException, ExecutionException {
		BigDecimal amount1 = new BigDecimal("2.01");
		BigDecimal amount2 = new BigDecimal("1.00");

		MessageIn messageIn1 = createMessageIn(ACC_NUM_1, ACC_NUM_2, amount1);
		MessageIn messageIn2 = createMessageIn(ACC_NUM_2, ACC_NUM_1, amount2);

		List<CompletableFuture<Void>> futures = new ArrayList<>();

		for (int i = 0; i < 1000; i++) {
			futures.add(transferCyclesWithException(messageIn1));
			futures.add(transferCyclesWithException(messageIn2));
		}

		for (CompletableFuture<Void> future : futures) {
			future.get();
		}

		assertTrue(exceptionCyclesCaughtNumber.get() >= 1);
	}

	private void transfer(int cyclesNum, MessageIn... messages) throws ExecutionException, InterruptedException {
		List<CompletableFuture<Void>> futures = new ArrayList<>();

		for (int i = 0; i < cyclesNum; i++) {
			for (MessageIn message : messages) {
				futures.add(transfer(message));
			}
		}

		for (CompletableFuture<Void> future : futures) {
			future.get();
		}
	}

	private CompletableFuture<Void> transfer(final MessageIn messageIn) {
		return CompletableFuture.supplyAsync(() -> {
			boolean exceptionCaught = false;

			try {
				service.transfer(messageIn);
			} catch (ServiceException e) {
				exceptionCaught = true;
			}

			assertFalse(exceptionCaught);

			return null;
		}, executor);
	}

	private CompletableFuture<Void> transferWithException(final MessageIn messageIn) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				service.transfer(messageIn);
			} catch (ServiceException e) {
				assertEquals(ServiceException.class, e.getClass());
				assertEquals("There is no enough money on account number " + ACC_NUM_1, e.getMessage());
				exceptionCaughtNumber.getAndIncrement();
			}

			return null;
		}, executor);
	}

	private CompletableFuture<Void> transferCyclesWithException(final MessageIn messageIn) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				service.transfer(messageIn);
			} catch (ServiceException e) {
				assertEquals(ServiceException.class, e.getClass());
				assertEquals("There is no enough money on account number " + ACC_NUM_1, e.getMessage());
				exceptionCyclesCaughtNumber.getAndIncrement();
			}

			return null;
		}, executor);
	}

	private void assertNoChanges() {
		assertEquals(BALANCE_1, getAccBalance(ACC_NUM_1));
		assertEquals(BALANCE_2, getAccBalance(ACC_NUM_2));
		assertEquals(BALANCE_3, getAccBalance(ACC_NUM_3));
		assertEquals(BALANCE_4, getAccBalance(ACC_NUM_4));
		assertEquals(BALANCE_5, getAccBalance(ACC_NUM_5));
		assertEquals(BALANCE_6, getAccBalance(ACC_NUM_6));
	}

	private Storage initAndFillData() {
		Storage storage = new StorageImpl();

		Account account1 = new Account();
		account1.setAccNum(ACC_NUM_1);
		account1.setBalance(BALANCE_1);
		storage.saveAccount(account1.getAccNum(), account1);

		Account account2 = new Account();
		account2.setAccNum(ACC_NUM_2);
		account2.setBalance(BALANCE_2);
		storage.saveAccount(account2.getAccNum(), account2);

		Account account3 = new Account();
		account3.setAccNum(ACC_NUM_3);
		account3.setBalance(BALANCE_3);
		storage.saveAccount(account3.getAccNum(), account3);

		Account account4 = new Account();
		account4.setAccNum(ACC_NUM_4);
		account4.setBalance(BALANCE_4);
		storage.saveAccount(account4.getAccNum(), account4);

		Account account5 = new Account();
		account5.setAccNum(ACC_NUM_5);
		account5.setBalance(BALANCE_5);
		storage.saveAccount(account5.getAccNum(), account5);

		Account account6 = new Account();
		account6.setAccNum(ACC_NUM_6);
		account6.setBalance(BALANCE_6);
		storage.saveAccount(account6.getAccNum(), account6);

		return storage;
	}

	private BigDecimal getAccBalance(long accNum) {
		return storage.getAccount(accNum).getBalance();
	}

	private MessageIn createMessageIn(long senderAccNum, long recipientAccNum, BigDecimal amount) {
		MessageIn messageIn = new MessageIn();
		messageIn.setRequestId(UUID.randomUUID().toString().replace("-", ""));
		messageIn.setSenderAccNum(senderAccNum);
		messageIn.setRecipientAccNum(recipientAccNum);
		messageIn.setAmount(amount);

		return messageIn;
	}
}
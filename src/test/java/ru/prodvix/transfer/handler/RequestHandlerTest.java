package ru.prodvix.transfer.handler;

import com.google.gson.JsonObject;
import org.junit.Test;
import ru.prodvix.transfer.model.MessageIn;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class RequestHandlerTest {

	private static RequestHandler handler = new RequestHandler();

	@Test
	public void testGetBodyAsString() throws IOException {
		String body = createBody("aa1", 1, 2, "234.114");

		byte[] bytes = body.getBytes();
		InputStream is = new ByteArrayInputStream(bytes);

		String result = handler.getBodyAsString(is);
		assertEquals(result, body);
	}

	@Test
	public void convertNegativeAccNum() {
		String body = createBody("aa1", -1, 2, "234.114");
		MessageIn msg = handler.convertFromJson(body);

		assertEquals("aa1", msg.getRequestId());
		assertEquals(-1, msg.getSenderAccNum());
		assertEquals(2, msg.getRecipientAccNum());
		assertEquals(new BigDecimal("234.114"), msg.getAmount());

		body = createBody("aa1", 1, -2, "234.114");
		msg = handler.convertFromJson(body);

		assertEquals("aa1", msg.getRequestId());
		assertEquals(1, msg.getSenderAccNum());
		assertEquals(-2, msg.getRecipientAccNum());
		assertEquals(new BigDecimal("234.114"), msg.getAmount());

		body = createBody("aa1", -1, -2, "234.114");
		msg = handler.convertFromJson(body);

		assertEquals("aa1", msg.getRequestId());
		assertEquals(-1, msg.getSenderAccNum());
		assertEquals(-2, msg.getRecipientAccNum());
		assertEquals(new BigDecimal("234.114"), msg.getAmount());
	}

	@Test
	public void convertAmountWithManyDecimalDigits() {
		String body = createBody("aa1", -1, 2, "100.99999999999999999999999999999999");
		MessageIn msg = handler.convertFromJson(body);

		assertEquals(new BigDecimal("100.99999999999999999999999999999999"), msg.getAmount());
	}

	@Test
	public void convertAmountWithManyZeroDecimalDigits() {
		String body = createBody("aa1", -1, 2, "100.000000000000000000000000000000");
		MessageIn msg = handler.convertFromJson(body);

		assertEquals(new BigDecimal("100.000000000000000000000000000000"), msg.getAmount());
	}

	@Test
	public void convertAmountWithManyZeroDecimalDigitsAndOne() {
		String body = createBody("aa1", -1, 2, "100.000000000000000000000000000001");
		MessageIn msg = handler.convertFromJson(body);

		assertEquals(new BigDecimal("100.000000000000000000000000000001"), msg.getAmount());
	}

	@Test
	public void convertNegativeAmount() {
		String body = createBody("aa1", -1, 2, "-100.000000000000000000000000000001");
		MessageIn msg = handler.convertFromJson(body);

		assertEquals(new BigDecimal("-100.000000000000000000000000000001"), msg.getAmount());
	}

	private String createBody(String requestId, long senderAccNum, long recipientAccNum, String amount) {
		JsonObject body = new JsonObject();
		body.addProperty("requestId", requestId);
		body.addProperty("senderAccNum", senderAccNum);
		body.addProperty("recipientAccNum", recipientAccNum);
		body.addProperty("amount", amount);

		return body.toString();
	}
}